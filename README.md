# Learn2JS
## Rapid Application Development
###### by Mark Damon Hughes

Some blog posts pointing in the right direction:

- [Hypercard! The Software Tool of Tomorrow!](https://mdhughes.tech/2018/03/10/hypercard-the-software-tool-of-tomorrow/)
- [The Mother of All Demos](https://mdhughes.tech/2018/12/10/the-mother-of-all-demos/)

Ideally, I want something as easy to program software in as [Atari BASIC on the Atari 800](https://youtu.be/_vK84lvwQwo), but with a decent modern language: JavaScript (ES6).

### Getting Started

- Setup: Install [npm](https://www.npmjs.com) to install node & electron, [multimarkdown](https://fletcherpenney.net/multimarkdown/) to generate documentation.
- To Run: clone or download it, `npm install`, edit `lib/testscript.js`, `npm start`. Type anything in the input box to show some demo junk.
- To Make Standalone Applications: `npm run build && npm run dist-mac` (or dist-windows or dist-linux), creates a dist folder.
- To Debug: In `npm start` mode, you have access to the console with Cmd-Opt-I

### Features

- Common application shell.
- Javascript script runs in a (probably-secure) sandbox.
- Help menu has a comprehensive user manual.

### To Do

- I don't really need patches and pull requests, but kindly file issues!
- File>Open, command-line, drag-and-drop, and double-click launching of scripts.
- Script editing? Maybe a simple editor, and then suggest Atom, etc. for real editing.
- UI editing, in the style of Hypercard: A page in a script draws to canvas (and can add overlay UI elements), clicks can transition to different pages.
- Database beyond localStorage.
- Multiple language support: BASIC, LOGO, Scheme, anything that can compile into JS.

### Components

- [Fira Sans](https://github.com/mozilla/Fira): Digitized data copyright (c) 2012-2015, The Mozilla Foundation and Telefonica S.A.
with Reserved Font Name Fira, This Font Software is licensed under the SIL Open Font License, Version 1.1.
