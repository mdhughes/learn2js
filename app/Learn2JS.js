/* Learn2JS.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { ArrayUtil } = require("./ArrayUtil.js");
const { MathUtil } = require("./MathUtil.js");
const { StringUtil } = require("./StringUtil.js");

const DEBUG = (location.hostname !== "localhost");
const HISTORY_MAX = 1000, HISTORY_DELETE = 100;
let STORAGE = true;

/** Shortcut for finding an HTML element by ID. */
function $(elid) {
	return document.getElementById(elid);
}

/** Logs to the console only if DEBUG is true. */
function DLOG() {
	if (DEBUG) {
		console.log.apply(this, arguments); // eslint-disable-line prefer-rest-params
	}
}

const kHiresTimer = true;
const kFPS = 60;

let LEARN2JS = null;

/** Keeps a rolling average of last 60 samples, .fps is the current value. */
class FPSCounter {
	constructor() {
		this.nsamples = 60;
		this.samples = ArrayUtil.dim(this.nsamples, 0);
		this.index = 0;
		this.total = 0;
		this.fps = 0;
	}

	/** Add a new sample, and get the average. */
	update(sample) {
		if ( ! Number.isFinite(sample)) {
			return 0;
		}
		this.total -= this.samples[this.index];
		this.total += sample;
		this.samples[this.index] = sample;
		this.index = (this.index + 1) % this.nsamples;
		this.fps = this.total/this.nsamples;
	}
}

//_____________________________________
/** Learn2JS interacts with HTML. */
class Learn2JS {
	/** Singleton instance */
	static app() {
		if ( ! LEARN2JS) {
			LEARN2JS = new Learn2JS();
		}
		return LEARN2JS;
	}

	constructor() {
		if ( ! this.storageAvailable() ) {
			STORAGE = false;
		}
		this.history = this.storageGet("history", []);
		this.historyIndex = this.storageGet("historyIndex", 0);

		this.hasInputFocus = false;
		this.keyState = {};
		this.mouseState = {
			buttons: 0,
			x: 0,
			y: 0,
			xdown: 0,
			ydown: 0,
			xup: 0,
			yup: 0,
		};

		this.fpsCounter = new FPSCounter();

		document.addEventListener("focusin", (evt)=>{this.focusChange(evt, true);}, false);
		document.addEventListener("focusout", (evt)=>{this.focusChange(evt, false);}, false);
		document.addEventListener("keydown", (evt)=>{this.keyChange(evt, true);}, false);
		document.addEventListener("keyup", (evt)=>{this.keyChange(evt, false);}, false);
		document.addEventListener("mousedown", (evt)=>{this.mouseChange(evt);}, false);
		document.addEventListener("mouseup", (evt)=>{this.mouseChange(evt);}, false);
		document.addEventListener("mousemove", (evt)=>{this.mouseChange(evt);}, false);
	}

	unload() {
		this.storageSet("history", this.history);
		this.storageSet("historyIndex", this.historyIndex);
	}

	focusChange(evt, state) {
		switch (evt.target.tagName) {
			case "INPUT":
			case "TEXTAREA":
				this.hasInputFocus = state;
				break;
			default:
				this.hasInputFocus = !state;
		}
		//DLOG("focusChange ", evt.target, evt, this.hasInputFocus);
		// clear all key events
		for (const k in this.keyState) {
			this.keyState[k] = false;
		}
	}

	keyChange(evt, state) {
		if ( ! (evt.defaultPrevented || this.hasInputFocus || evt.metaKey)) {
			const k = evt.key.toLowerCase();
			//DLOG( (this.hasInputFocus?"INPUT":"APP")+": keyChange "+k+" "+state, evt);
			this.keyState[k] = state;
			evt.preventDefault();
		}
	}

	mouseChange(evt) {
		if (evt.defaultPrevented) {
			return;
		}
		//DLOG(this, "mouseChange ", evt);
		const x = evt.x, y = evt.y;
		if (this.mouseState.buttons === 0 && evt.buttons !== 0) {
			this.mouseState.xdown = x;
			this.mouseState.ydown = y;
		} else if (this.mouseState.buttons !== 0 && evt.buttons === 0) {
			this.mouseState.xup = x;
			this.mouseState.yup = y;
		}
		this.mouseState.buttons = evt.buttons;
		this.mouseState.x = x;
		this.mouseState.y = y;
	}

	/** Requests a document from a url, passes (result, null) or (null, errorMessage) to callback. */
	ajax(method, url, data, callback) {
		DLOG("ajaxRequest "+method+" "+url+", data="+data);
		const request = new XMLHttpRequest();
		if ( ! request) {
			const msg = "No XMLHttpRequest";
			console.error(msg);
			alert(msg);
			return false;
		}
		request.onreadystatechange = ()=>{
			DLOG("readyStateChange "+request.url+", state "+request.readyState);
			if (request.readyState !== XMLHttpRequest.DONE) {
				return false;
			} else if ((request.status === 200 || request.status === 0) && ! request.responseText.startsWith("#!")) {
				return callback(request.responseText, null);
			} else {
				const msg = url+" returned "+request.status+" "+request.statusText+"\n"+request.responseText;
				return callback(null, msg);
			}
		};
		request.open(method, url, true);
		if (data) {
			request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			request.send(jsonToFormEncoded(data));
		} else {
			request.send();
		}
	}

	/** Calls `testFunction`, and if the result is truthy (not false, "", null, or undefined),
	* throws an error with the text of the function and message `msg`.
	* Asserts are best used as preconditions to a function, or to verify the results of something that "can't fail".
	* A minimal example is `Learn2JS.app().assert(()=>y, x);` where y is the result of an operation on x.
	* console.assert used to do this in Node, but then they broke it to not throw, like wussy web browsers.
	*/
	assert(testFunction, msg) {
		if ( ! testFunction()) {
			throw new Error("Assert Failed: "+ testFunction.toString().replace(/\s+/, " ")+ (msg ? ": "+msg : "") );
		}
	}

	/** Adds `line` to `Learn2JS.history`, which will be preserved between runs, up to HISTORY_MAX lines.
	* Empty lines and duplicates of the previous are ignored.
	* Returns true if the line was added.
	*/
	historyAdd(line) {
		//DLOG("historyAdd", line);
		if (line.length === 0 || ArrayUtil.last(this.history) === line) {
			return false;
		}
		this.history.push(line);
		this.historyIndex = this.history.length;
		if (this.history.length > HISTORY_MAX) {
			this.history.splice(0, HISTORY_DELETE);
			this.historyIndex -= HISTORY_DELETE;
		}
		return true;
	}

	/** Modifies history index by `delta` and returns that line from history; or "" if past end of history. */
	historyGet(delta) {
		this.historyIndex = MathUtil.inBounds(this.historyIndex + delta, 0, this.history.length);
		const line = this.history[this.historyIndex] || "";
		//DLOG("historyGet", delta, line);
		return line;
	}

	/** Returns the contents of the input element. */
	input() {
		return $("input").value.trim();
	}

	/** Appends `text` to the input element. */
	inputAppend(text) {
		const inp = $("input");
		const newtext = inp.value + text;
		inp.innerHTML = newtext;
		inp.value = newtext;
	}

	/** Clears the input element. */
	inputClear() {
		const inp = $("input");
		inp.innerHTML = "";
		inp.value = "";
	}

	inputFocus() {
		$("input").focus();
	}

	/** Assigns to the input element. */
	inputSet(text) {
		const inp = $("input");
		inp.innerHTML = text;
		inp.value = text;
	}

	/** Changes the prompt above the input area. */
	inputPrompt(text) {
		$("inputPrompt").innerHTML = text;
	}

	/** Clears the output element. */
	outputClear() {
		this.outputColors("white", "black");
		$("output").innerHTML = "";
	}

	/** Sets the output element's colors. */
	outputColors(fgcolor, bgcolor) {
		$("output").style = "color: "+fgcolor+"; background-color: "+bgcolor+";";
	}

	/** Writes an exception and message to output, see StringUtil.errorHTML(). */
	outputError(e, msg) {
		this.output(StringUtil.errorHTML(e, msg));
	}

	/** Writes msg to the output element. */
	output(msg) {
		const out = $("output");
		out.innerHTML += msg;
		const atBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;
		if ( ! atBottom) {
			out.scrollTop = out.scrollHeight - out.clientHeight;
		}
	}

	/** Returns true if localStorage is available. */
	storageAvailable() {
		try {
			const testValue = "__storage_test__";
			const storage = window["localStorage"];
			localStorage[testValue] = testValue;
			const s = storage[testValue];
			storage.removeItem(testValue);
			if (s === testValue) {
				return true;
			} else {
				console.error("localStorage failed: "+s);
				return false;
			}
		} catch (e) {
			console.error("localStorage not available: "+e);
			return false;
		}
	}

	/** Returns value for `key` from localStorage, or `defaultValue` (default null) if it was not stored. */
	storageGet(key, defaultValue=null) {
		if ( ! STORAGE) {
			return defaultValue;
		}
		const text = localStorage[key];
		if (text && text !== "undefined") {
			try {
				return JSON.parse(text);
			} catch (e) {
				console.error("Error storageGet '"+key+"':", text, e);
				return defaultValue;
			}
		} else {
			return defaultValue;
		}
	}

	/** Saves `data` for `key` in localStorage.
	* data must be JSON types only.
	*/
	storageSet(key, data) {
		if ( ! STORAGE) {
			return false;
		}
		try {
			const text = JSON.stringify(data);
			localStorage[key] = text;
			return true;
		} catch (e) {
			console.error("Error storageSet '"+key+"'", data, e);
			return false;
		}
	}

	/** Activates a 60 FPS timer, which calls `callback` every time. Sets `timerTimestamp` and `timerDelta` properties. */
	timerStart(callback) {
		this.timerCallback = callback;
		if (kHiresTimer) {
			this.timerTimestamp = performance.now()-1;
			this._timerTickHires(this.timerTimestamp);
		} else {
			this.timerTimestamp = Date.now()-1;
			this.timer = setInterval(()=>{ this._timerTick(Date.now()); }, Math.ceil(1000/kFPS));
		}
	}

	/** Internal method for timer */
	_timerTick(timestamp) {
		this.timerDelta = timestamp - this.timerTimestamp;
		this.fpsCounter.update(1000/this.timerDelta);
		this.timerTimestamp = timestamp;
		this.timerCallback(timestamp);
	}

	/** Internal method for hires timer */
	_timerTickHires(timestamp) {
		this.timer = requestAnimationFrame((timestamp)=>{this._timerTickHires(timestamp);});
		this._timerTick(timestamp);
	}

	/** Cancel the timer. You should do this on stopping the application, but also whenever it loses focus and shouldn't be doing anything. */
	timerStop() {
		if (this.timer) {
			if (kHiresTimer) {
				cancelAnimationFrame(this.timer);
			} else {
				clearInterval(this.timer);
			}
		}
	}

} // Learn2JS

module.exports = { $, DEBUG, DLOG, Learn2JS, FPSCounter };
