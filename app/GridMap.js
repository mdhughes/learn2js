/* GridMap.js
 * Learn2JS
 * Copyright © 2019 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { $, DEBUG, DLOG, Learn2JS } = require("./Learn2JS.js");
const { Canvas } = require("./Canvas.js");

class Dir {
	static forValue(value) {
		const d = Dir.VALUES[value];
		Learn2JS.app().assert(()=>d, value);
		return d;
	}

	constructor(value, name, delta) {
		this.value = value;
		this.name = name;
		this.delta = delta;
		if ( ! Dir.VALUES) {
			Dir.VALUES = [];
		}
		Dir.VALUES.push(this);
	}

	backDir() {
		return Dir.forValue( (this.value+2) % 4 );
	}

	leftDir() {
		return Dir.forValue( (this.value+3) % 4 );
	}

	rightDir() {
		return Dir.forValue( (this.value+1) % 4 );
	}

	toString() {
		return "[Dir "+this.name+"]";
	}
} // Dir

Dir.N = new Dir(0, "N", [0, -1]);
Dir.E = new Dir(1, "E", [1, 0]);
Dir.S = new Dir(2, "S", [0, 1]);
Dir.W = new Dir(3, "W", [-1, 0]);

//______________________________________
class Terrain {
	static forCh(ch) {
		const ter = Terrain.VALUES[ch];
		Learn2JS.app().assert(()=>ter, ch);
		return ter;
	}

	static forName(name) {
		const ter = Terrain.VALUES[name];
		Learn2JS.app().assert(()=>ter, name);
		return ter;
	}

	static preloadImages() {
		for (const ter of Object.values(Terrain.VALUES)) {
			Canvas.getImage(ter.frame[0]);
		}
	}

	constructor(ch, name, blocking, opaque, frame) {
		this.ch = ch;
		this.name = name;
		this.blocking = blocking;
		this.opaque = opaque;
		this.frame = frame;

		if ( ! Terrain.VALUES) {
			Terrain.VALUES = {};
		}
		Terrain.VALUES[ch] = this;
		Terrain.VALUES[name] = this;
	}

	toString() {
		return "["+this.ch+" "+this.name+" "+(this.blocking ? "B" : "NB")+"]";
	}
} // Terrain

new Terrain('?', "unknown",	true, false,	["../assets/i/custom32.png",160,32,32,32] ); // magenta
new Terrain('.', "grass",	false, false,	["../assets/i/dg/dg_grounds32.png",0,32,32,32] );
new Terrain('h', "hill",	false, false,	["../assets/i/dg/dg_grounds32.png",96,320,32,32] );
new Terrain('M', "mountain",	true, true,	["../assets/i/dg/dg_grounds32.png",0,416,32,32] );
new Terrain('~', "water",	true, false,	["../assets/i/dg/dg_grounds32.png",0,64,32,32] );
new Terrain('=', "waterDeep",	true, false,	["../assets/i/dg/dg_grounds32.png",64,64,32,32] );
new Terrain(':', "sand",	false, false,	["../assets/i/dg/dg_grounds32.png",96,32,32,32] );
new Terrain('%', "swamp",	false, true,	["../assets/i/dg/dg_grounds32.png",0,224,32,32] );
new Terrain('#', "wall",	true, true,	["../assets/i/dg/dg_dungeon32.png",0,0,32,32] );
new Terrain('_', "floor",	false, false,	["../assets/i/custom32.png",64,0,32,32] ); // lightgray
new Terrain('+', "door",	true, true,	["../assets/i/dg/dg_dungeon32.png",0,32,32,32] );
new Terrain('-', "doorOpen",	false, false,	["../assets/i/dg/dg_dungeon32.png",32,32,32,32] );
new Terrain('s', "secret",	true, true,	["../assets/i/dg/dg_dungeon32.png",32,0,32,32] ); // darker wall
new Terrain('S', "secretFound",	false, true,	["../assets/i/dg/dg_dungeon32.png",192,224,32,32] );
new Terrain('^', "stairsUp",	false, false,	["../assets/i/dg/dg_dungeon32.png",0,64,32,32] );
new Terrain('v', "stairsDown",	false, false,	["../assets/i/dg/dg_dungeon32.png",32,64,32,32] );

// only used as features, overlay on terrain

new Terrain('0', "pit",		false, false,	["../assets/i/dg/dg_misc32.png",160,256,32,32] );
new Terrain('1', "pitFound",	false, false,	["../assets/i/dg/dg_misc32.png",32,160,32,32] );
new Terrain('2', "inn",		false, false,	["../assets/i/dg/dg_iso32.png",223,593,40,40] );
new Terrain('3', "village",	false, false,	["../assets/i/dg/dg_iso32.png",169,591,40,40] );
new Terrain('4', "castle",	false, false,	["../assets/i/dg/dg_iso32.png",166,636,48,48] );
new Terrain('5', "cave",	false, false,	["../assets/i/dg/dg_iso32.png",548,545,40,40] );
new Terrain('6', "dungeon",	false, false,	["../assets/i/dg/dg_dungeon32.png",192,0,32,32] );
new Terrain('7', "ruin",	false, false,	["../assets/i/dg/dg_edging332.png",32,288,32,32] );
new Terrain('8', "fountain",	false, false,	["../assets/i/dg/dg_edging332.png",64,288,32,32] );
new Terrain('9', "gravestone",	false, false,	["../assets/i/dg/dg_edging332.png",64,256,32,32] );
new Terrain('t', "tree",	false, true,	["../assets/i/dg/dg_edging332.png",224,288,32,32] );
new Terrain('T', "treeAutumn",	false, true,	["../assets/i/dg/dg_edging332.png",192,320,32,32] );
new Terrain('p', "treePine",	false, true,	["../assets/i/dg/dg_edging332.png",192,288,32,32] );
new Terrain('P', "treePineSnow", false, true,	["../assets/i/dg/dg_edging332.png",224,320,32,32] );
new Terrain('r', "treePalm",	false, true,	["../assets/i/dg/dg_edging332.png",160,288,32,32] );
new Terrain('R', "treeDead",	false, true,	["../assets/i/dg/dg_edging332.png",96,288,32,32] );
new Terrain('c', "cactus",	false, true,	["../assets/i/dg/dg_edging332.png",128,288,32,32] );
new Terrain('/', "diagNE",	true, true,	["../assets/i/dg/dg_iso32.png",228,353,32,32] );
new Terrain('\\', "diagSE",	true, true,	["../assets/i/dg/dg_iso32.png",280,353,32,32] );

new Terrain('┃', "path_ns",	false, false,	["../assets/i/dg/dg_edging332.png",192,96,32,32] );
new Terrain('━', "path_ew",	false, false,	["../assets/i/dg/dg_edging332.png",224,96,32,32] );
new Terrain('┏', "path_es",	false, false,	["../assets/i/dg/dg_edging332.png",128,128,32,32] );
new Terrain('┳', "path_esw",	false, false,	["../assets/i/dg/dg_edging332.png",160,288,32,32] );
new Terrain('┓', "path_sw",	false, false,	["../assets/i/dg/dg_edging332.png",192,288,32,32] );
new Terrain('┣', "path_nes",	false, false,	["../assets/i/dg/dg_edging332.png",224,128,32,32] );
new Terrain('╋', "path_nesw",	false, false,	["../assets/i/dg/dg_edging332.png",0,160,32,32] );
new Terrain('┫', "path_nsw",	false, false,	["../assets/i/dg/dg_edging332.png",32,160,32,32] );
new Terrain('┗', "path_ne",	false, false,	["../assets/i/dg/dg_edging332.png",64,160,32,32] );
new Terrain('┻', "path_new",	false, false,	["../assets/i/dg/dg_edging332.png",96,160,32,32] );
new Terrain('┛', "path_nw",	false, false,	["../assets/i/dg/dg_edging332.png",128,160,32,32] );
new Terrain('║', "river_ns",	false, false,	["../assets/i/dg/dg_edging332.png",0,0,32,32] );
new Terrain('═', "river_ew",	false, false,	["../assets/i/dg/dg_edging332.png",32,0,32,32] );
new Terrain('╔', "river_es",	false, false,	["../assets/i/dg/dg_edging332.png",192,0,32,32] );
new Terrain('╦', "river_esw",	false, false,	["../assets/i/dg/dg_edging332.png",224,0,32,32] );
new Terrain('╗', "river_sw",	false, false,	["../assets/i/dg/dg_edging332.png",0,32,32,32] );
new Terrain('╠', "river_nes",	false, false,	["../assets/i/dg/dg_edging332.png",32,32,32,32] );
new Terrain('╬', "river_nesw",	false, false,	["../assets/i/dg/dg_edging332.png",64,32,32,32] );
new Terrain('╣', "river_nsw",	false, false,	["../assets/i/dg/dg_edging332.png",96,32,32,32] );
new Terrain('╚', "river_ne",	false, false,	["../assets/i/dg/dg_edging332.png",128,32,32,32] );
new Terrain('╩', "river_new",	false, false,	["../assets/i/dg/dg_edging332.png",160,32,32,32] );
new Terrain('╝', "river_nw",	false, false,	["../assets/i/dg/dg_edging332.png",192,32,32,32] );
new Terrain('╪', "bridge_ns",	false, false,	["../assets/i/dg/dg_edging332.png",128,224,32,32] );
new Terrain('╫', "bridge_ew",	false, false,	["../assets/i/dg/dg_edging332.png",224,224,32,32] );

//______________________________________
class Grid {
	/** t: Terrain, f: Terrain feature (may be null) */
	constructor(t, f) {
		console.assert(t, "Expected terrain, but got ", t);
		this.t = t;
		this.f = f;
	}

	toString() {
		return this.t.ch+ (this.f ? this.f.ch : this.t.ch);
	}

	isBlocking() {
		return this.t.blocking || (this.f && this.f.blocking);
	}

	draw(canvas, srect) {
		canvas.drawImageFrame(this.t.frame, srect);
		if (this.f) {
			canvas.drawImageFrame(this.f.frame, srect);
		}
		// canvas.setColor("green");
		// canvas.drawRect(srect);
		// canvas.drawTextMono(this.toString(), 10, "green", [srect[0]+16, srect[1]+16]);
	}

} // Grid

//______________________________________
class GridMap {
	constructor(size) {
		this.setSize(size);
	}

	setSize(size) {
		this.size = size;
		this.grids = [];
		const t = Terrain.forName("unknown");
		for (let y = 0; y < size[1]; ++y) {
			for (let x = 0; x < size[0]; ++x) {
				const g = new Grid(t, null);
				this.grids.push(g);
			}
		}
		this.startPt = [Math.floor(size[0]/2), Math.floor(size[1]/2)];
	}

	toString() {
		let s = "";
		for (let y = 0; y < this.size[1]; ++y) {
			for (let x = 0; x < this.size[0]; ++x) {
				const g = this.gridAt([x,y]);
				s += g.toString();
			}
			s += "\n";
		}
		return s;
	}

	fillTerrain(rect, t, f) {
		for (let y = 0; y < this.size[1]; ++y) {
			for (let x = 0; x < this.size[0]; ++x) {
				const g = this.gridAt([x,y]);
				g.t = t;
				if (f !== undefined) {
					g.f = f;
				}
			}
		}
	}

	/** Calls f(pt, g, h) on every grid in the map. h is a histogram of nearby terrains. */
	filter(f) {
		for (let y = 0; y < this.size[1]; ++y) {
			for (let x = 0; x < this.size[0]; ++x) {
				const h = {};
				for (let dy = -1; dy <= 1; ++dy) {
					for (let dx = -1; dx <= 1; ++dx) {
						const g = this.gridAt([x+dx, y+dy]);
						if (g) {
							h[g.t] = (h[g.t] || 0) + 1;
						}
					}
				}
				const pt = [x,y];
				const g = this.gridAt(pt);
				f(this, pt, g, h);
			}
		}
	}

	gridAt(pt) {
		if ( ! this.inBounds(pt)) {
			//DLOG("out of bounds at", pt);
			return null;
		}
		const i = Math.floor(pt[0]) + Math.floor(pt[1]) * this.size[0];
		return this.grids[i];
	}

	inBounds(pt) {
		return pt[0] >= 0 && pt[0] < this.size[0] && pt[1] >= 0 && pt[1] < this.size[1];
	}

	/* Returns a list of directions which have unconnected neighbor cells. */
	unconnectedDirs(pt) {
		const unconnected = [];
		for (let d = 0; d < 4; ++d) {
			const dir = Dir.forValue(d);
			const g1 = this.gridAt([pt[0] + dir.delta[0], pt[1] + dir.delta[1]]);
			const g2 = this.gridAt([pt[0] + dir.delta[0]*2, pt[1] + dir.delta[1]*2]);
			if (g1 && g1.isBlocking() && g2 && g2.isBlocking()) {
				unconnected.push(d);
			}
		}
		return unconnected;
	}

	draw(canvas, origin, tileSize) {
		DLOG("GridMap.draw", canvas, origin, tileSize);
		canvas.setColor("gray");
		canvas.fillRect([origin[0], origin[1], this.size[0] * tileSize, this.size[1] * tileSize]);
		for (let y = 0; y < this.size[1]; ++y) {
			for (let x = 0; x < this.size[0]; ++x) {
				const g = this.gridAt([x,y]);
				const srect = [origin[0] + x * tileSize, origin[1] + y * tileSize, tileSize, tileSize];
				g.draw(canvas, srect);
			}
		}
	}

} // GridMap

module.exports = { Dir, Terrain, Grid, GridMap };
