/* Test.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { $ } = require("./Learn2JS.js");
const { StringUtil } = require("./StringUtil.js");

/** Call Test.start(), then a series of Test.assert, Test.equal, etc., then Test.finish(), will print a report at the bottom of the page.
*/
class Test {
	static start() {
		if ( ! $("testconsole")) {
			document.body.innerHTML += "<pre id='testconsole' style='position: fixed; bottom: 0; background: red; color: white'></pre>";
		}
		document.assertsPassed = 0;
		document.assertsFailed = 0;
	}

	static finish() {
		if ($("testconsole")) {
			const msg = document.assertsPassed+" tests passed, "+document.assertsFailed+" tests failed";
			if (document.assertsFailed === 0) {
				$("testconsole").style.background = "green";
				setTimeout(()=>{ Test.reset(); }, 3000);
			}
			$("testconsole").innerHTML += StringUtil.htmlify(msg);
			console.log(msg);
		}
	}

	static reset() {
		if ($("testconsole")) {
			$("testconsole").remove(1);
		}
	}

	static message(msg) {
		console.assert(msg);
		if ($("testconsole")) {
			$("testconsole").innerHTML += StringUtil.htmlify(msg)+"\n";
			console.error(msg);
		}
	}

	static assert(cond, msg) {
		if (cond) {
			if (document.assertsPassed !== undefined) {
				document.assertsPassed += 1;
			}
		} else {
			if (document.assertsFailed !== undefined) {
				document.assertsFailed += 1;
			}
			msg = msg || "Assertion failed";
			Test.message(msg);
		}
	}

	static assertException(func, errtype, msg) {
		try {
			func();
			Test.assert(false, msg);
		} catch (e) {
			if (e instanceof errtype) {
				Test.assert(true, msg);
			} else {
				Test.assert(false, "Expected exception "+errtype+", but got "+e + (msg ? ": "+msg : ""));
			}
		}
	}

	static equals(expected, butgot, msg) {
		Test.assert(expected === butgot, "Expected ((("+expected+"))) but got ((("+butgot+")))" + (msg ? ": "+msg : ""));
	}

} // Test

module.exports = { Test };
