/* Index.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { ipcRenderer, remote } = require("electron");
const app = remote.app;
const fs = require("fs");
const path = require("path");

const kUseVMSandbox = false;
const { NodeVM, VMScript } = require("vm2");

const { $, DEBUG, DLOG, Learn2JS } = require("./Learn2JS.js");
const { Test } = require("./Test.js");
const { ArrayUtil } = require("./ArrayUtil.js");
const { AudioPlayer } = require("./AudioPlayer.js");
const { Canvas } = require("./Canvas.js");
const { Dice } = require("./Dice.js");
const { MathUtil } = require("./MathUtil.js");
const { StringUtil } = require("./StringUtil.js");

class Index {

	/** Non-sandboxed script loading. */
	static requireSource(source, sandbox) {
		// const module = {};
		// (function (module) {
		// 	eval(source); // eslint-disable-line no-eval
		// }());
		// return module;

		/*
		// simplified CommonJS:
		require.cache = Object.create(null);
		function require(name) {
			if (!(name in require.cache)) {
				let code = readFile(name);
				let module = {exports: {}};
				require.cache[name] = module;
				let wrapper = Function("require, exports, module", code);
				wrapper(require, module.exports, module);
			}
			return require.cache[name].exports;
		}
		*/

		const module = {exports: {}};
		const wrapper = Function("require, exports, module", source); // eslint-disable-line no-new-func
		wrapper(require, module.exports, module);
		return module.exports;
	}

	/** Run when the page finishes loading. */
	init() {
		DLOG("init");
		Learn2JS.app(); // load instance

		// Unit tests—passing in Test so other modules don't have to require it
		if (DEBUG) {
			Test.start();
			ArrayUtil.test(Test);
			MathUtil.test(Test);
			StringUtil.test(Test);
			Test.finish();
		}

		this.doCatalog();
	}

	/** Run when the page is unloaded. */
	unload() {
		DLOG("unload");
		if (this.script) {
			this.unloadScript();
		}
		Learn2JS.app().unload();
	}

	getLibPath() {
		return path.join(app.getAppPath(), "lib");
	}

	getUserPath() {
		return app.getPath("userData");
	}

	handleInputEvent(event) {
		if ( ! (event.metaKey || event.altKey || event.ctrlKey || event.shiftKey) && event.key === "Enter" ) {
			event.preventDefault();
			this.sendInput();
		} else if (event.altKey && event.key === "Enter") {
			Learn2JS.app().inputAppend("\n");
		} else if (event.altKey && event.key === "ArrowUp") {
			Learn2JS.app().inputSet( Learn2JS.app().historyGet(-1) );
		} else if (event.altKey && event.key === "ArrowDown") {
			Learn2JS.app().inputSet( Learn2JS.app().historyGet(1) );
		} else if (event.key === "Escape") {
			event.preventDefault();
			Learn2JS.app().inputSet("");
		}
	}

	/** Send button, or key equivalent. */
	sendInput() {
		const line = Learn2JS.app().input();
		Learn2JS.app().inputSet("");
		Learn2JS.app().historyAdd(line);
		this.main(line);
	}

	unloadScript() {
		// unload existing script
		if (this.script.unload) {
			this.script.unload();
		}
		this.vm = null;
		this.script = null;
		this.scriptFilename = null;
	}

	doCatalog() {
		function collectFlinksFrom(flinks, filepath) {
			for (const f of fs.readdirSync(filepath)) {
				if (f.endsWith(".js")) {
					const fname = path.basename(f, ".js");
					flinks.push( "<button onclick='index.doLoad(\""+fname+"\");'>"+fname+"</button>" );
				}
			}
		}
		const flinks = [];
		collectFlinksFrom(flinks, this.getLibPath());
		collectFlinksFrom(flinks, this.getUserPath());
		Learn2JS.app().output(StringUtil.columnate(flinks, 2));
	}

	doLoad(filename) {
		Learn2JS.app().output("Load "+filename+"\n");

		if (this.script) {
			this.unloadScript();
		}

		// only two legal paths for scripts: argv[0]/lib, or ~/Library/Application Support/Learn2JS (or OS equivalent)
		filename = path.basename(filename);
		if ( ! filename.endsWith(".js")) {
			filename += ".js";
		}
		this.scriptFilename = path.join(this.getLibPath(), filename);
		if ( ! fs.existsSync(this.scriptFilename)) {
			this.scriptFilename = path.join(this.getUserPath(), filename);
			if ( ! fs.existsSync(this.scriptFilename)) {
				this.scriptFilename = null;
			}
		}
		if ( ! this.scriptFilename) {
			Learn2JS.app().outputError(null, "No such file "+filename+" in "+this.getLibPath()+" or "+this.getUserPath());
			return false;
		}

		// vm2 creates a probably-secure sandbox, with only the listed globals, and no access to require.
		// https://github.com/patriksimek/vm2
		if (kUseVMSandbox) {
			const sandbox = {
				document, setTimeout, setInterval, setImmediate,
				$, DEBUG, DLOG, Learn2JS, ArrayUtil, AudioPlayer, Canvas, Dice, MathUtil, StringUtil,
			};
			this.vm = new NodeVM({
				console: "inherit",
				sandbox: sandbox,
				require: {
					external: false,
					builtin: [],
					root: this.getLibPath(),
				}
			});
		}

		let scriptText = fs.readFileSync(this.scriptFilename, {encoding:"utf8"});
		DLOG("Loaded", this.scriptFilename, ":", scriptText.length, "bytes");
		scriptText = '"use strict";\n' + scriptText + '\n';

		// script object contains all module.exports, expected to have at least main, and optionally init, unload.
		try {
			if (kUseVMSandbox) {
				this.script = this.vm.run( new VMScript(scriptText) );
			} else {
				this.script = Index.requireSource(scriptText);
			}
			if (this.script && this.script.init) {
				this.script.init();
			}
		} catch (e) {
			this.script = null;
			console.error(e);
			Learn2JS.app().outputError(e, "Error loading "+this.scriptFilename);
		}
		return true;
	}

	/** Run every time the Run button is clicked, with the contents of the input area. */
	main(line) {
		DLOG("main", line);
		if (line === "/catalog") {
			this.doCatalog();
			return;
		} else if (line.startsWith("/load ")) {
			this.doLoad(line.substr(6));
			return;
		}

		if (this.script && this.script.main) {
			try {
				this.script.main(line);
			} catch (e) {
				console.error(e);
				Learn2JS.app().outputError(e, "Error running "+this.scriptFilename);
			}
		} else {
			Learn2JS.app().output(StringUtil.htmlify(line+"\n"));
		}
	}

} // class Index

module.exports = { Index };
