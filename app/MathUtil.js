/* MathUtil.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

/** Points are 2-element arrays [x, y]. Rects are 4-element arrays [x, y, w, h]. */
class MathUtil {

	/** Limits `x` to minimum `a`, maximum `b`. */
	static inBounds(x, a, b) {
		return Math.max(a, Math.min(x, b));
	}

	static pointAdd(a, b) {
		return [a[0] + b[0], a[1] + b[1]];
	}

	static pointCopy(a) {
		return [a[0], a[1]];
	}

	static pointDist(a, b) {
		const xterm = a[0] - b[0], yterm = a[1] - b[1];
		return Math.sqrt(xterm * xterm + yterm * yterm); // TODO: should I approximate or is native math faster?
	}

	static pointEquals(a, b) {
		return a[0] === b[0] && a[1] === b[1];
	}

	static pointSubtract(a, b) {
		return [a[0] - b[0], a[1] - b[1]];
	}

	/** Creates a rect, 4-element array, from point `origin` and size `size`, both 2-element arrays. */
	static rectMake(origin, size) {
		return [origin[0], origin[1], size[0], size[1]];
	}

	static rectCopy(a) {
		return [a[0], a[1], a[2], a[3]];
	}

	/** Expands all sides of a rect by n. */
	static rectExpand(a, n) {
		return [a[0]-n, a[1]-n, a[2]+n*2, a[3]+n*2];
	}

	/** Returns a rect similar to a which fits within b. */
	static rectFitInsideRect(a, b) {
		const c = a.slice();
		if (c[0] < b[0]) {
			c[0] = b[0];
		}
		if (c[0] + c[2] > b[0] + b[2]) {
			c[0] = b[0] + b[2] - c[2];
		}
		if (c[1] < b[1]) {
			c[1] = b[1];
		}
		if (c[1] + c[3] > b[1] + b[3]) {
			c[1] = b[1] + b[3] - c[3];
		}
		return c;
	}

	/** Returns true if `outer` contains `pt`. */
	static rectContainsPoint(outer, pt) {
		return pt[0] >= outer[0] && pt[1] >= outer[1] && pt[0] < outer[0]+outer[2] && pt[1] < outer[1]+outer[3];
	}

	/** Returns true if `outer` completely encloses `inner`. */
	static rectContainsRect(outer, inner) {
		return inner[0] >= outer[0] && inner[1] >= outer[1] && inner[0]+inner[2] <= outer[0]+outer[2] && inner[1]+inner[3] <= outer[1]+outer[3];
	}

	/** Returns true if rects `a` and `b` have any intersection. */
	static rectIntersectsRect(a, b) {
		// const aleft = a[0], atop = a[1], aright = a[0]+a[2], abottom = a[1]+a[3];
		// const bleft = b[0], btop = b[1], bright = b[0]+b[2], bbottom = b[1]+b[3];
		//return aleft < bright && aright > bleft && atop < bbottom && abottom > btop;
		return a[0] < b[0]+b[2] && a[0]+a[2] > b[0] && a[1] < b[1]+b[3] && a[1]+a[3] > b[1];
	}

	static rectWithRadius(center, radius) {
		return [center[0]-radius, center[1]-radius, radius*2+1, radius*2+1];
	}


	/** Returns the largest power of 2 which is <= n */
	static roundPowerOf2(n) {
		let pos = 0;
		while (n > 0) {
			++pos;
			n >>= 1;
		}
		return 1<<(pos-1);
	}

	static test(Test) {
		Test.equals(""+[1, 50, 100], ""+[-1, 50, 999].map((x)=>{return MathUtil.inBounds(x, 1, 100);} ) );
	}

} // MathUtil

module.exports = { MathUtil };
