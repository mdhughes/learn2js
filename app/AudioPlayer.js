/* AudioPlayer.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

let AUDIO = null;

class AudioPlayer {
	/** Returns the audio player instance, creating it if necessary. */
	static audio() {
		if ( ! AUDIO) {
			AUDIO = new AudioPlayer();
		}
		return AUDIO;
	}

	// Internal constructor, do not create new AudioPlayers!
	constructor() {
		this.audioContext = new (window.AudioContext || window.webkitAudioContext)();
		this.audioCache = {};
		this.playing = {};
	}

	/** Play a URL's contents, looping indefinitely if 'loop' is true.
	* Only WAV and MP3 files are supported across browsers and platforms, and MP3 may not play on some Linux systems.
	*/
	play(src, loop) {
		const buffer = this.audioCache[src];
		if (buffer) {
			this.playAudioBuffer(src, Boolean(loop));
		} else {
			const req = new XMLHttpRequest();
			req.open("GET", src, true);
			req.responseType = "arraybuffer";
			req.onload = ()=>{
				this.audioContext.decodeAudioData(req.response, (buffer)=>{
					this.audioCache[src] = buffer;
					this.playAudioBuffer(src, loop);
				},
				(e)=>{
					console.error("Error decoding "+src+": "+e);
				});
			};
			req.send();
		}
	}

	// Internal function to play a loaded buffer.
	playAudioBuffer(src, loop) {
		const audioSource = this.audioContext.createBufferSource();
		audioSource.buffer = this.audioCache[src];
		audioSource.loop = loop;
		audioSource.connect(this.audioContext.destination);
		audioSource.start(0);
		this.playing[src] = audioSource;
	}

	/** Stops a playing sound by URL. */
	stop(src) {
		const audioSource = this.playing[src];
		if (audioSource) {
			audioSource.stop(0);
			delete this.playing[src];
		}
	}

}

module.exports = { AudioPlayer };
