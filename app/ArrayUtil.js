/* ArrayUtil.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

class ArrayUtil {

	/** Generic comparison for sorting. */
	static compare(a, b) {
		if (Array.isArray(a) && Array.isArray(b)) {
			for (let i = 0, minlen = Math.min(a.length, b.length); i < minlen; ++i) {
				if (a[i] < b[i]) {
					return -1;
				} else if (a[i] > b[i]) {
					return 1;
				}
			}
			return ArrayUtil.compare(a.length, b.length);
		}
		// unit compare
		if (a < b) {
			return -1;
		} else if (a > b) {
			return 1;
		} else {
			return 0;
		}
	}

	/** Returns an array filled with value (default 0). */
	static dim(len, value=0) {
		const a = Array(len);
		a.fill(value);
		return a;
	}

	/** Returns first element in array, or null if empty array. */
	static first(arr) {
		return arr.length >= 1 ? arr[0] : null;
	}

	/** Returns last element in array, or null if empty array. */
	static last(arr) {
		return arr.length >= 1 ? arr[arr.length-1] : null;
	}

	/** Returns an array filled with numbers from min to max (not inclusive), by step (default 1). */
	static range(min, max, step=1) {
		if (min > max) {
			const tmp = min;
			min = max;
			max = tmp;
		}
		const count = (step === 1) ? Math.ceil(max-min) : Math.ceil((max-min)/step);
		const arr = Array(count);
		for (let i = 0, n = min; i < count && n < max; ++i, n += step) {
			arr[i] = n;
		}
		return arr;
	}

	/** Removes 'value' from array and returns it if it was found, otherwise returns null. */
	static remove(arr, value) {
		const i = arr.indexOf(value);
		if (i >= 0) {
			arr.splice(i, 1);
			return value;
		}
		return null;
	}

	static test(Test) {
		Test.equals(""+[-1, 0, 1, -1, 0, 1], ""+[[1, 6], [6, 6], [6, 1], ["bar", "foo"], ["quux", "quux"], ["quux", "foo"]].map((p)=>{return ArrayUtil.compare(p[0], p[1]);} ) );

		const scores = [ [0,0,"nobody"], [220,0,"bob"], [220,500,"ba"], [34,1000,"dave"], [34,1000,"sara"]];
		scores.sort(ArrayUtil.compare);
		DLOG(scores);
		Test.equals("nobody", scores[0][2]);
		Test.equals("dave", scores[1][2]);
		Test.equals("sara", scores[2][2]);
		Test.equals("bob", scores[3][2]);
		Test.equals("ba", scores[4][2]);

		Test.equals(""+[1, 15, 115], ""+[1, 15, 115].sort(ArrayUtil.compare));
		Test.equals(""+[6, 6, 6], ""+ArrayUtil.dim(3, 6));
		Test.equals(1, ArrayUtil.first([1, 2, 3]));
		Test.equals(3, ArrayUtil.last([1, 2, 3]));
		Test.equals(""+[1, 3, 5], ""+ArrayUtil.range(1, 6, 2));
		const rmarray = [1, 2, 3, 4, 5];
		Test.equals(2, ArrayUtil.remove(rmarray, 2));
		Test.equals(""+[1, 3, 4, 5], ""+rmarray);
	}

} // ArrayUtil

module.exports = { ArrayUtil };
