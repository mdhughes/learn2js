/* Canvas.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { $, DLOG } = require("./Learn2JS.js");
const { ArrayUtil } = require("./ArrayUtil.js");
const { MathUtil } = require("./MathUtil.js");

const kFontSans = "Fira Sans", kFontMono = "Fira Mono";

let CANVAS = null;

/** General-purpose display sprite to draw on canvas.
 */
class Sprite {
	// TODO: title, can be shown at various position, color, font
	// TODO: overlay sub-sprites

	static colorForHealthFraction(hper) {
		if (hper >= 0.9) {
			return "green";
		} else if (hper >= 0.75) {
			return "yellow";
		} else if (hper >= 0.25) {
			return "orange";
		} else {
			return "red";
		}
	}

	constructor(name, origin, size) {
		this.name = name;
		this.origin = MathUtil.pointCopy(origin);
		this.size = MathUtil.pointCopy(size);
		this.color = null; // set to fill in background of sprite
		this.frameIndex = 0; // change to animate or show state
		this.frames = [];
		this.speed = 2;
	}

	addFrame(img, sx, sy, sw, sh) {
		this.frames.push( [img, sx, sy, sw, sh] );
	}

	bounds() {
		return MathUtil.rectMake(this.origin, this.size);
	}

	draw() {
		const canvas = Canvas.canvas();
		const bounds = this.bounds();
		if (this.color) {
			canvas.setColor(this.color);
			canvas.fillRect(bounds);
		}
		const frame = this.frames[this.frameIndex];
		if (frame) {
			canvas.drawImageFrame(frame, bounds);
		} else if ( ! this.color) {
			// error, invisible sprite
			canvas.setColor("red");
			canvas.drawRect(bounds);
		}

		if (this.health && this.healthMax) {
			const hper = this.health / this.healthMax;
			if (hper < 1.0) {
				canvas.setColor(Sprite.colorForHealthFraction(hper));
				canvas.fillRect([this.origin[0], this.origin[1], Math.ceil(this.size[0] * hper), 4]);
				canvas.drawRect([this.origin[0], this.origin[1], this.size[0], 4]);
			}
		}
	}
}

/** Call Canvas.canvas() to return the main canvas.
 *
 * .element: DOM element
 * .context: CanvasRenderingContext2D. A few convenience methods are defined, but most drawing can be done with context: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
 * .keyState: Keyboard state. keyState[k] is true if pressed. `k` is lowercased version of KeyEvent.key.
 * .inputFocus: Does the text input area have focus? Useful to pause when waiting for the user to enter text.
 */
class Canvas {
	/** Activates the canvas, and returns the canvas object.
	* If w, h are given, and the canvas does not exist, it will be created with that size. Default is 1024,512
	*/
	static canvas(size=[1024, 512]) {
		if ( ! CANVAS) {
			CANVAS = new Canvas("canvas", size);
		}
		return CANVAS;
	}

	/** Loads an image and returns it, has a permanent cache for faster lookup.
	* Images may not be loaded fully on first request; pre-load images from init if you want them to show up!
	*/
	static getImage(filename) {
		if ( ! Canvas._imageCache) {
			Canvas._imageCache = {};
		}
		let image = Canvas._imageCache[filename];
		if (image) {
			return image;
		}
		image = new Image();
		image.src = filename;
		Canvas._imageCache[filename] = image;
		return image;
	}

	/** Hides the canvas. */
	static reset() {
		DLOG("Canvas.reset");

		const el = $("canvas");
		if (el) {
			el.remove(1);
		}
		CANVAS = null;
	}

	constructor(elid, size) {
		DLOG("new Canvas", elid, size[0], size[1]);
		this.element = $(elid);
		if ( ! this.element) {
			$("output").insertAdjacentHTML("beforebegin", "<div class='canvasPanel'><canvas id='"+elid+"' width='"+size[0]+"' height='"+size[1]+"' tabindex='0'></canvas></div>");
			this.element = $(elid);
			this.element.focus();
		}
		this.context = this.element.getContext("2d");
		this.context.webkitImageSmoothingEnabled = false;
		this.context.imageSmoothingEnabled = false;
		this.element.hidden = false;
		// make logical size equal display size
		const displaySize = this.canvasSize();
		this.element.width = displaySize[0];
		this.element.height = displaySize[1];
	}

	/** Returns the size of the canvas element. */
	canvasSize() {
		return [this.element.offsetWidth, this.element.offsetHeight];
	}

	/** Convenience method to call context.clearRect given the canvasSize. */
	clear() {
		const sz = this.canvasSize();
		this.context.clearRect(0, 0, sz[0], sz[1]);
	}

	/** Draws an ellipse on the canvas within a rectangle's bounds. */
	drawEllipse(rect) {
		this.context.beginPath();
		this.context.ellipse(rect[0] + rect[2]/2, rect[1] + rect[3]/2, rect[2]/2, rect[3]/2, 0, 0, 2*Math.PI);
		this.context.stroke();
	}

	/** Loads an image, and draws it on the canvas when it's loaded.
	* `pt`: Location to draw at, in original size.
	* Note, the image may not be immediately available, so don't rely on drawing order!
	*/
	drawImage(filename, pt) {
		this.context.drawImage(Canvas.getImage(filename), pt[0], pt[1]);
	}

	/** Loads an image, and draws it on the canvas when it's loaded.
	* `rect`: Display region to position & scale into.
	*/
	drawImageScaled(filename, rect) {
		this.context.drawImage(Canvas.getImage(filename), rect[0], rect[1], rect[2], rect[3]);
	}

	/** Loads an image, and draws it on the canvas when it's loaded.
	* `frame`: 5-element array: image filename, source x, y, w, h.
	* `rect`: Display region to position & scale into.
	*/
	drawImageFrame(frame, rect) {
		this.context.drawImage(Canvas.getImage(frame[0]), frame[1], frame[2], frame[3], frame[4], rect[0], rect[1], rect[2], rect[3]);
	}

	/** Draws a single line from p1 to p2 in stroke color. */
	drawLine(p1, p2) {
		this.context.beginPath();
		this.context.moveTo(p1[0], p1[1]);
		this.context.lineTo(p2[0], p2[1]);
		this.context.stroke();
	}

	/** Convenience method to call context.strokeRect */
	drawRect(rect) {
		this.context.strokeRect(rect[0], rect[1], rect[2], rect[3]);
	}

	/** Draws text in Fira monospace font. */
	drawTextMono(text, textSize, color, pt, align="center", baseline="alphabetic") {
		this.drawTextFont(text, textSize+"pt "+kFontMono, color, pt, align, baseline);
	}

	/** Draws text in Fira sans font. */
	drawTextSans(text, textSize, color, pt, align="center", baseline="alphabetic") {
		this.drawTextFont(text, textSize+"pt "+kFontSans, color, pt, align, baseline);
	}

	/** Draws text in any given font, color, at coords x, y, align may be left, right, or center. */
	drawTextFont(text, font, color, pt, align="center", baseline="alphabetic") {
		this.context.font = font;
		this.context.fillStyle = (color !== "black") ? "black" : "white";
		this.context.textAlign = align;
		this.context.textBaseline = baseline;

		for (let dy = -1; dy <= 1; dy += 2) {
			for (let dx = -1; dx <= 1; dx += 2) {
				this.context.fillText(text, pt[0] + dx, pt[1] + dy);
			}
		}
		this.context.fillStyle = color;
		this.context.fillText(text, pt[0], pt[1]);
	}

	/** Same as drawEllipse, but fills the region. */
	fillEllipse(color, rect) {
		this.context.beginPath();
		this.context.ellipse(rect[0] + rect[2]/2, rect[1] + rect[3]/2, rect[2]/2, rect[3]/2, 0, 0, 2*Math.PI);
		this.context.fill();
	}

	/** Convenience method to call context.fillRect */
	fillRect(rect) {
		this.context.fillRect(rect[0], rect[1], rect[2], rect[3]);
	}

	/** Sets the canvas fill & stroke style ("#RRGGBB" or any HTML/CSS color). */
	setColor(color) {
		this.context.strokeStyle = color;
		this.context.fillStyle = color;
	}

} // Canvas

module.exports = { Canvas, Sprite };
