/* main-electron.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const electron = require("electron");
const app = electron.app;
const fs = require("fs");
const path = require("path");
const zlib = require("zlib");
const DEBUG = require("electron-is-dev");

/** Logs to the console only if DEBUG is defined. */
function DLOG() {
	if (DEBUG) {
		console.log.apply(this, arguments); // eslint-disable-line prefer-rest-params
	}
}
DLOG("DEBUG");

// debug keys
// Cmd+Alt+I: DevTools
// Cmd+R: Reload
// Cmd+Sh+C: Element Inspector
require("electron-debug")();

let mainWindow; // prevent GC

const kMDHAppCopyright = "Copyright ©2018 by Mark Damon Hughes. All Rights Reserved.";

// https://developer.apple.com/reference/appkit/nsapplication/1428479-orderfrontstandardaboutpanelwith?language=objc
const kMDHAppCredits = "https://mdhughes.tech/\n"+
		"Node.js "+process.versions.node+", "+
		"Chromium "+process.versions.chrome+", "+
		"Electron "+process.versions.electron+".\n"+
		"Fonts from Mozilla Fira: https://github.com/mozilla/Fira";

const kMDHAppWidth = 1024;
const kMDHAppHeight = 768;

function createMenuTemplate() {
	const template = [];
	if (process.platform === "darwin") {
		template.push({ label: app.getName(),
			submenu: [
				{ role: "about", },
				{ type: "separator" },
				{
					label: "Preferences",
					accelerator: "Cmd+,",
					click: ()=>doPreferences()
				},
				{ type: "separator" },
				{
					role: "services",
					submenu: []
				},
				{ type: "separator" },
				{ role: "hide" },
				{ role: "hideothers" },
				{ role: "unhide" },
				{ type: "separator" },
				{ role: "quit" }
			]
		});
	}

	template.push({ label: "File",
		submenu: [
			{
				label: "New File",
				accelerator: "CmdOrCtrl+N",
				click: ()=>doNewFile()
			},
			{
				label: "Open File...",
				accelerator: "CmdOrCtrl+O",
				click: ()=>doOpenFile()
			},
			{
				label: "Save File...",
				accelerator: "CmdOrCtrl+S",
				click: ()=>doSaveFile()
			},
			{ type: "separator" },
			{ role: "close" }
		]
	});
	if (process.platform === "linux" || process.platform === "win32") {
		template[template.length-1].submenu.push(
			{ type: "separator" },
			{
				label: "Quit",
				click: ()=>doQuit()
			}
		);
	}

	template.push({ label: "Edit",
		submenu: [
			{ role: "undo" },
			{ role: "redo" },
			{ type: "separator" },
			{ role: "cut" },
			{ role: "copy" },
			{ role: "paste" },
			{ role: "selectall" }
		]
	});
	if (process.platform === "linux" || process.platform === "win32") {
		template[template.length-1].submenu.push(
			{ type: "separator" },
			{
				label: "Preferences",
				accelerator: "CmdOrCtrl+,",
				click: ()=>doPreferences()
			}
		);
	}

	if (process.platform === "darwin") {
		template.push({
			role: "window",
			submenu: [
				{ role: "minimize" },
				{ type: "separator" },
				{ role: "front" }
			]
		});
	}

	template.push({ label: "Help",
		role: "help",
		submenu: [
			{
				label: "User Manual",
				accelerator: "CmdOrCtrl+H",
				click: ()=>doHelp()
			},
		]
	});
	if (process.platform === "linux" || process.platform === "win32") {
		template[template.length-1].unshift(
			{
				label: "About "+app.getName(),
				click: ()=>doAbout()
			}
		);
	}

	return template;
}

function createMainWindow() {
	const windowFile = path.join(app.getPath("userData"), "window.json");
	const defaultWindowData = {bounds: {width:kMDHAppWidth, height:kMDHAppHeight}};
	fs.access(windowFile, fs.constants.R_OK, (err)=>{
		if (err) {
			console.error("window file "+windowFile+" not accessible");
			createMainWindowWithData(defaultWindowData);
		} else {
			fs.readFile(windowFile, "utf8", (err, data)=>{
				if (err) {
					console.error("Error reading "+windowFile+": "+err);
					createMainWindowWithData(defaultWindowData);
				} else {
					createMainWindowWithData( JSON.parse(data) );
				}
			});
		}
	});
}

function createMainWindowWithData(windowData) {
	mainWindow = new electron.BrowserWindow({ title: app.getName(),
		x: windowData.bounds.x, y: windowData.bounds.y,
		width: windowData.bounds.width, height: windowData.bounds.height,
		resizable: true,
	});

	mainWindow.loadURL(`file://${__dirname}/index.html`);

	mainWindow.on("close", function(evt) {
		electron.dialog.showMessageBox(mainWindow, {type:"warning", title:"Quit",
			message:"Quit - have you saved lately?", buttons:["Quit", "Cancel"],},
		(response)=>{
			if (response === 0) {
				const windowData = {"bounds": mainWindow.getBounds()};
				const windowFile = path.join(app.getPath("userData"), "window.json");
				fs.writeFileSync(windowFile, JSON.stringify(windowData));
				DLOG("close, saved "+JSON.stringify(windowData));
				mainWindow.destroy();
				mainWindow = null;
				app.quit();
				return true;
			}
		});
		evt.preventDefault();
		return false;
	});

	mainWindow.on("closed", function() {
		DLOG("closed");
		mainWindow = null;
	});

	const menu = electron.Menu.buildFromTemplate(createMenuTemplate());
	electron.Menu.setApplicationMenu(menu);
}

function createApplication() {
	DLOG("createApplication");

	app.commandLine.appendSwitch('js-flags', '--max-old-space-size=4096');

	if (process.platform === "darwin") {
		app.setAboutPanelOptions({
			applicationName: app.getName(),
			applicationVersion: app.getVersion(),
			version: app.getVersion(),
			copyright: kMDHAppCopyright,
			credits: kMDHAppCredits,
		});
	}

	app.on("window-all-closed", ()=>{
		if (process.platform === "darwin") {
			return false;
		} else {
			return doQuit();
		}
	});

	app.on("activate", ()=>{
		if ( ! mainWindow) {
			mainWindow = createMainWindow();
		}
	});

	app.on("ready", ()=>{
		if ( ! mainWindow) {
			mainWindow = createMainWindow();
		}
	});
}

// for Linux & Windows
function doAbout() {
	alert( app.getName()+" ("+app.getVersion()+")\n"+kMDHAppCopyright+"\n"+kMDHAppCredits+"\n" );
}

function doHelp() {
	const win = new electron.BrowserWindow({width:800, height:800});
	const helpFile = `file://${__dirname}/help.html`;
	win.loadURL(helpFile);
}

function doOpenFile() {
	DLOG("Open File");
	// FIXME:
}

function doNewFile() {
	DLOG("New File");
	// FIXME:
}

function doPreferences() {
	DLOG("Preferences");
	// FIXME:
}

function doQuit() {
	DLOG("Quit, bounds="+mainWindow.getBounds()); // FIXME
	app.quit();
	return true;
}

function doSaveFile() {
	DLOG("Save File");
	// FIXME:
}

createApplication();

module.exports = {
	DEBUG,
	DLOG,
	kMDHAppCopyright,
	kMDHAppCredits,
};
