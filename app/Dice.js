/* Dice.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

class Dice {

	/** Returns a random point along a rectangle [x, y, w, h]. */
	static boxSide(rect) {
		const sideRoll = Dice.rnd(4);
		switch (sideRoll) {
			case 0: return [ rect[0] + Dice.rnd(rect[2]), rect[1] ]; // north
			case 1: return [ rect[0] + rect[2]-1, rect[1] + Dice.rnd(rect[3]) ]; // east
			case 2: return [ rect[0] + Dice.rnd(rect[2]), rect[1] + rect[3]-1 ]; // south
			case 3: return [ rect[0], rect[1] + Dice.rnd(rect[3]) ]; // west
		}
	}

	/** Returns a single item from array. */
	static chooseFromArray(arr) {
		if (arr.length === 0) {
			return null;
		}
		const i = Math.floor(Math.random() * arr.length);
		return arr[i];
	}

	/** Returns the roll of 'n' dice of 's' sides, e.g. dice(3, 6) returns 3-18. */
	static d(n, s) {
		n = Math.ceil(n); s = Math.ceil(s);
		if (n <= 0 || s <= 0) {
			return 0;
		}
		let t = 0;
		for (let i = 0; i < n; ++i) {
			t += Dice.rnd(s) + 1;
		}
		return t;
	}

	/** Returns the roll of 1 die of 's' sides. */
	static die(s) {
		s = Math.ceil(s);
		if (s <= 0) {
			return 0;
		}
		return Dice.rnd(s) + 1;
	}

	/** Choose from `table` array of [chance, value] entries,
	* rolls against total chance if `roll` is not given.
	*/
	static diceTable(table, roll=null) {
		let t = 0;
		for (let i = 0; i < table.length; ++i) {
			t += table[i][0];
		}
		if (t <= 0) {
			console.error("Invalid dice table "+table);
			return null;
		}
		if ( ! roll) {
			roll = die(t);
		}
		let c = 0;
		for (let i = 0; i < table.length; ++i) {
			c += table[i][0];
			if (roll <= c) {
				return table[i][1];
			}
		}
		console.error("Invalid roll "+roll+" on diceTable "+table);
		return null;
	}

	/** Given data, which must be an array of arrays of strings; merges random elements from the subarrays & returns that as a name. */
	static randomName(data) {
		let name = "";
		for (let i = 0; i < data.length; ++i) {
			const j = Dice.rnd(data[i].length);
			name += data[i][j];
		}
		return name;
	}

	/** Returns a random integer from 0 to n-1. */
	static rnd(n) {
		return Math.floor(Math.random() * n);
	}

	/** Fisher-Yates */
	static shuffleArray(arr) {
		for (let i = arr.length-1; i >= 1; --i) {
			const j = Dice.rnd(i+1);
			const tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}
		return arr;
	}

} // class Dice

//module.exports = { Dice };
module.exports.Dice = Dice;
