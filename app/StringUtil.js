/* StringUtil.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

class ParseError extends Error {
}

class StringUtil {

	/** Returns an HTML table containing lines distributed over columns, vertically first. */
	static columnate(lines, columns) {
		let text = "<table width='100%'><tr>";
		let count = 0;
		const rows = Math.ceil(lines.length / columns);
		for (const s of lines) {
			if (count % rows === 0) {
				text += "<td>";
			} else {
				text += "<br>";
			}
			text += s;
			text += "\n";
			count += 1;
		}
		text += "</table>";
		return text;
	}

	/** Returns an ISO8601-formatted date/time for a Date `d`. */
	static dateFormat(d) {
		let s = d.toISOString().replace("T", " ");
		if (s.endsWith(".000Z")) {
			s = s.substring(0, s.length-5);
		}
		return s;
	}

	static errorHTML(e, msg) {
		return "<div class='error'>"+(e ? StringUtil.htmlify(e) : "")+
			(e && msg ? ": " : "")+
			(msg ? StringUtil.htmlify(msg) : "" )+ "</div>\n";
	}

	/** Returns a string equivalent of floating point number x, with a given number of decimal places (default 3),
	* with trailing '0' chars removed.
	*/
	static ftoa(x, places=3) {
		return x.toFixed(places).replace(/\.?0*$/, "");
	}

	/** Translate plain text into legal HTML. */
	static htmlify(text) {
		return String(text).replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/\n/, "<br>");
	}

	/** Simple debugging tool, if you aren't sure what's in obj: StringUtil.htmlify(StringUtil.objectToString(obj)) */
	static objectToString(obj) {
		let text = "";
		const keys = Object.keys(obj).sort();
		for (const k of keys) {
			text += String(k)+": "+String(obj[k])+"\n";
		}
		return text;
	}

	/** Pads start of obj.toString() with padchars (default nbsp) so it fills n chars. */
	static padLeft(obj, n, padchars="\xa0") {
		return String(obj).padStart(n, padchars);
	}

	/** Pads end of string s with padchars (default nbsp) so it fills n chars. */
	static padRight(obj, n, padchars="\xa0") {
		return String(obj).padEnd(n, padchars);
	}

	/** Returns a boolean equivalent of text, or throws an error.
	* Accepted values (case-insensitive): true, t, yes, on, 1, false, f, no, off, 0
	*/
	static parseBoolean(text) {
		const tlower = text.trim().toLowerCase();
		if (tlower === "true" || tlower === "t" || tlower === "yes" || tlower === "on" || tlower === "1") {
			return true;
		} else if (tlower === "false" || tlower === "f" || tlower === "no" || tlower === "off" || tlower === "0") {
			return false;
		}
		throw new ParseError("'"+text+"' is not boolean!");
	}

	/** Returns an integer equivalent of text, or throws an error.
	* If minValue and/or maxValue are given, any value outside that range throws an error.
	*/
	static parseInteger(text, minValue, maxValue) {
		const n = parseInt(text.trim(), 10);
		if (Number.isNaN(n)) {
			throw "'"+text+"' is not an integer!";
		}
		if ( (minValue !== undefined && n < minValue) || (maxValue !== undefined && n > maxValue) ) {
			throw new ParseError(n+" must be in the range "+minValue+"…"+maxValue);
		}
		return n;
	}

	/** Provides a very simplistic English plural of 'n' units of 's'.
	* 1 plum, 2 plums, 1 quartz, 2 quartzes, 1 bunny, 2 bunnies.
	*/
	static plural(n, s) {
		let text = ""+n+" "+s;
		if (n !== 1) {
			if (s.endsWith("y")) {
				text = text.substr(0, text.length-1) + "ies";
			} else if (s.endsWith("x") || s.endsWith("z")) {
				text += "es";
			} else {
				text += "s";
			}
		}
		return text;
	}

	/** Returns a name with all chars except letters, digits, ', - replaced with _, and runs of _ replaced with one.
	* If the result is empty, returns defaultName.
	*/
	static purifyName(name, defaultName) {
		if (name) {
			name = name.replace(/[^\w'-]/g, "_").replace(/__+/g, "_");
			if (name.length <= 1 || name === "_") {
				return defaultName+" "+name;
			} else if (name.length > 16) {
				name = name.substr(0, 16);
			}
			return name;
		} else {
			return defaultName;
		}
	}

	/** Splits words in `s` at any whitespace and removes empty words. */
	static splitWords(s) {
		return s.split(/\s+/).filter((s)=>{ return s.length > 0; });
	}

	/** Uppercases only the first character of a string. */
	static toCapitalized(s) {
		if (s && s.length >= 1) {
			return s[0].toUpperCase() + s.substr(1);
		} else {
			return "";
		}
	}

	static test(Test) {
		Test.equals("<table width='100%'><tr><td>one\n<br>two\n<br>three\n<td>four\n<br>five\n<br>six\n<td>seven\n<br>eight\n</table>",
			StringUtil.columnate(["one", "two", "three", "four", "five", "six", "seven", "eight"], 3) );
		Test.equals("<div class='error'>Error: wtf: stuff</div>\n", StringUtil.errorHTML(new Error("wtf"), "stuff"));
		Test.equals("1970-06-21 00:00:00", StringUtil.dateFormat(new Date((5*30+21)*86400*1000)) );
		Test.equals("3.142", StringUtil.ftoa(Math.PI));
		Test.equals("-66.67", StringUtil.ftoa(-66.666, 2));
		Test.equals("&lt;escaped &amp; safe&gt;", StringUtil.htmlify("<escaped & safe>"));
		Test.equals("a: 1\nb: 2\n", StringUtil.objectToString({a:1,b:2}));
		Test.equals("..hello", StringUtil.padLeft("hello", 7, "."));
		Test.equals("hello..", StringUtil.padRight("hello", 7, "."));
		Test.equals(""+[true, true, true, true, true, false, false, false, false, false], ""+["true", "t", "yes", "on", "1", "false", "f", "no", "off", "0"].map(StringUtil.parseBoolean));
		Test.equals(""+[50, 1, 100], ""+["50", "1", "100"].map((x)=>{return StringUtil.parseInteger(x, 1, 100);} ) );
		Test.assertException(()=>{return StringUtil.parseInteger("-1", 1, 100);}, ParseError);
		Test.equals(""+["1 plum", "2 plums", "1 quartz", "2 quartzes", "1 bunny", "2 bunnies"], ""+[[1, "plum"], [2, "plum"], [1, "quartz"], [2, "quartz"], [1, "bunny"], [2, "bunny"]].map((p)=>{return StringUtil.plural(p[0], p[1]);} ) );
		Test.equals("Capitol capital", StringUtil.toCapitalized("capitol capital"));
	}

} // StringUtil

module.exports = { StringUtil, ParseError };
