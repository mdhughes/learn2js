Title: Learn2JS: Help
Author: Mark Damon Hughes
Date: 2018-12-20 00:47
CSS: ../assets/fira-ttf.css
CSS: ../assets/style.css
HTML header:

# Learn2JS
###### by Mark Damon Hughes

## Input

The input box starts out with 3 lines, but can be resized smaller with [-] or larger with [+] buttons.

Pressing [Send] or hitting Return will send the contents to the running script. You can insert newlines in the input box with Alt-Return.

Hitting Alt-Up or Alt-Down will move through history, the last 1000 lines are saved permanently.

There are several special commands starting with a `/`, which will always work in any script:

Command			|Effect
--------		|:------
/catalog		|List all known scripts in app lib or user data. You can click the name to load it.
/load FILENAME		|Load a script by name.

## Scripts

Scripts are either stored in the application's `lib` folder (not user-accessible in a built binary), or in user data:

OS	|Location
--------|:------
Mac	|`~/Library/Application Support/Learn2JS`
Windows	|`%APPDATA%/Learn2JS`
Linux	|`$XDG_CONFIG_HOME` or `~/.config`

The script should look something like:

```
/* SCRIPTNAME
 * AUTHOR
 * COPYRIGHT
 * LICENSE
 */
/* eslint-env node */

"use strict";

const { $, DEBUG, DLOG, Learn2JS } = require("./Learn2JS.js");
const { ArrayUtil } = require("./ArrayUtil.js");
const { AudioPlayer } = require("./AudioPlayer.js");
const { Canvas } = require("./Canvas.js");
const { Dice } = require("./Dice.js");
const { MathUtil } = require("./MathUtil.js");
const { StringUtil } = require("./StringUtil.js");

function init() {
}

function unload() {
}

function main(line) {
}

module.exports = { init, main, unload };
```

You'll fill in your own SCRIPTNAME, AUTHOR, COPYRIGHT, LICENSE text. Make a common piece of text for this, and just change the filename. Now you can share your code safely.

The eslint comments will let eslint check the script. The script must be in "use strict" mode (it will be whether you type that or not).

The requires are the standard library of Learn2JS. You can import anything from `npm`, but these are "batteries included" and deliberately simple APIs. In the future these may be preloaded, and you can remove the requires.

The three functions are the lifecycle of the script:

- `init` will be run when the script is first loaded. At a minimum, you may want to do something like:
	```
	Canvas.reset(); // no canvas needed
	Canvas.canvas().clear(); // OR set up canvas
	Learn2JS.app().outputClear();
	Learn2JS.app().output("<h2>SCRIPT NAME</h2>"));
	Learn2JS.app().output("<div>INSTRUCTIONS</div>"));
	```
- `unload` will be run just before the script is unloaded.
- `main` will be run every time the user enters text and hits [Send].

All three are optional; if you take one out, make sure to remove it from `module.exports`.

Any additional functions and globals in your script are only visible inside the script.

<!-- Scripts run with a very limited set of functionality: Standard JS library, the Learn2JS libraries listed above, and `document` from HTML. They should be as secure as running a web page, there is no local file access. -->
