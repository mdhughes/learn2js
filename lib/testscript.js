/* testscript.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { $, DEBUG, DLOG, Learn2JS } = require("./Learn2JS.js");
const { ArrayUtil } = require("./ArrayUtil.js");
const { AudioPlayer } = require("./AudioPlayer.js");
const { Canvas } = require("./Canvas.js");
const { Dice } = require("./Dice.js");
const { MathUtil } = require("./MathUtil.js");
const { StringUtil } = require("./StringUtil.js");

function init() {
	// preload images
	Canvas.getImage("../assets/i/Hand_with_Reflecting_Sphere.jpg");

	Canvas.reset();
	Learn2JS.app().outputClear();
	Learn2JS.app().output("<p>testscript init</p>\n");
	Learn2JS.app().output("<p>Type some text and hit Return!</p>\n");
	Learn2JS.app().inputFocus();
}

function unload() {
	Learn2JS.app().output("<p>testscript unload</p>\n");
}

function main(line) {
	Learn2JS.app().output(StringUtil.htmlify(line.toUpperCase()+"\n"));
	// make some noise
	const audio = AudioPlayer.audio();
	audio.play("../assets/s/diceclatter.wav");

	// pick a random web-palette color
	const canvas = Canvas.canvas();
	const hexcolors = ["00", "33", "66", "99", "cc", "ff"];
	const c = "#"+Dice.chooseFromArray(hexcolors)+ Dice.chooseFromArray(hexcolors)+ Dice.chooseFromArray(hexcolors);
	canvas.setColor(c);

	// draw a skein
	const sideLength = Math.min.apply(null, canvas.canvasSize());
	canvas.drawRect([0, 0, sideLength, sideLength]);
	for (let i = 0; i < sideLength; i += 16) {
		canvas.drawLine([0, i], [i, sideLength]);
		canvas.drawLine([i, 0], [sideLength, i]);
	}
	// original is 258x386, so clipping a square region into the canvas.
	canvas.drawImageFrame(["../assets/i/Hand_with_Reflecting_Sphere.jpg", 0, 0, 258, 258], [128, 128, 256, 256]);

	// modify the web page
	document.getElementById("output").style = "background-color: "+c+";";

	// find out everything wikiquote knows about towels.
	Learn2JS.app().ajax("GET", "https://en.wikiquote.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy", null, (html, error)=>{
		if (error) {
			console.error(error);
			return;
		}
		const text = html.replace(/<[^>]*>/g, ""); // strip all markup
		const firstTowel = text.indexOf("towel");
		const previousNewline = text.lastIndexOf("\n", firstTowel); // line before towel
		const lastTowel = text.lastIndexOf("towel");
		const nextNewline = text.indexOf("\n", lastTowel);
		Learn2JS.app().output(StringUtil.htmlify(text.substr(previousNewline, nextNewline-previousNewline+1)));
	});
}

module.exports = { init, main, unload };
