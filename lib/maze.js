/* maze.js
 * Learn2JS
 * Copyright © 2018,2019 Mark Damon Hughes
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { $, DEBUG, DLOG, Learn2JS } = require("./Learn2JS.js");
const { ArrayUtil } = require("./ArrayUtil.js");
const { AudioPlayer } = require("./AudioPlayer.js");
const { Canvas, Sprite } = require("./Canvas.js");
const { Dice } = require("./Dice.js");
const { MathUtil } = require("./MathUtil.js");
const { StringUtil } = require("./StringUtil.js");
const { Dir, Terrain, Grid, GridMap } = require("./GridMap.js");

const kMazeError = "Invalid maze command, type 'help' for instructions.";

const kMazeHelp = `<table>
	<tr><td>maze WIDTH HEIGHT	<td>Creates a maze with given size (must be odd).
	<tr><td>forest WIDTH HEIGHT	<td>Creates a forest with rivers and paths.
	<tr><td>island WIDTH HEIGHT	<td>Creates an island by particle deposition.
	<tr><td>world WIDTH HEIGHT	<td>Creates a fractal world, islands in an ocean.
	<tr><td>diag WIDTH HEIGHT	<td>Create a random diagonal 'maze' with no paths.
	<tr><td>redraw			<td>Redraw previous map as tiles.
	<tr><td>text			<td>Redraw previous map as text.
	<tr><td>menu			<td>Redraw dropdown menu.
	<tr><td>help			<td>Gives these instructions again.
</table>`;

const kMazeUI = `<table>
<tr><td><label for='mazetype'>Maze Type:</label>
	<td><select id='mazetype' name='mazetype'>
		<option value='maze'>maze with given size (must be odd)</option>
		<option value='forest'>forest with rivers and paths</option>
		<option value='island'>island by particle deposition</option>
		<option value='world'>fractal world, islands in an ocean</option>
		<option value='diag'>random diagonal 'maze' with no paths</option>
		</select>
<tr><td><label for='mazewidth'>Size:</label></td>
	<td><input id='mazewidth' type='text' name='mazewidth' size='4' value='16' />,
		<input id='mazeheight' type='text' name='mazeheight' size='4' value='16' />
<tr><td><button onclick='index.main( $("mazetype").value+" "+$("mazewidth").value+" "+$("mazeheight").value )'>Create</button>
	<td>&nbsp;
</table>`;

let LAST_MAP = null;

function generateMaze(givenSize) {
	// size must be odd, and doesn't always generate right under 7 grids.
	const size = MathUtil.pointCopy(givenSize);
	if (size[0] % 2 === 0) {
		--size[0];
	}
	if (size[0] < 7) {
		size[0] = 7;
	}
	if (size[1] % 2 === 0) {
		--size[1];
	}
	if (size[1] < 7) {
		size[1] = 7;
	}
	if (size[0] !== givenSize[0] || size[1] !== givenSize[1]) {
		Learn2JS.app().output("<div>Resizing to "+size[0]+","+size[1]+"</div>\n");
	}
	const map = new GridMap(size);
	const terFloor = Terrain.forName("floor");
	const terWall = Terrain.forName("wall");

	map.fillTerrain([0, 0, size[0], size[1]], terWall);
	map.startPt = [Dice.rnd(Math.floor((size[0]-2)/2))*2+1, 1]; // north
	const endRow = size[1]-2;
	let endPt = null;

	// connect all rooms (odd-numbered indices), through tunnels (even-numbered indices)
	let pt = map.startPt;
	for (;;) {
		const g = map.gridAt(pt);
		const dirs = map.unconnectedDirs(pt);
		//DLOG("step", pt, ", dirs=", dirs);
		if (dirs.length > 0) {
			const d = Dice.chooseFromArray(dirs);
			const dir = Dir.forValue(d);
			// corridor
			const pt1 = [pt[0] + dir.delta[0], pt[1] + dir.delta[1] ];
			map.gridAt(pt1).t = terFloor;
			// next room
			const pt2 = [pt[0] + dir.delta[0]*2, pt[1] + dir.delta[1]*2 ];
			map.gridAt(pt2).t = terFloor;
			pt = pt2;
			if (pt[1] === endRow) {
				endPt = pt;
			}
		} else { // pick a new connected room with unconnected neighbors and try again
			const connected = [];
			for (let y = 1; y <= size[1]-1; y += 2) {
				for (let x = 1; x <= size[0]-1; x += 2) {
					const pt2 = [x, y];
					const g2 = map.gridAt(pt2);
					if (g2.t === terFloor && map.unconnectedDirs(pt2).length > 0) {
						connected.push(pt2);
					}
				}
			}
			if (connected.length > 0) {
				pt = Dice.chooseFromArray(connected);
				//DLOG("teleport to", pt);
			} else {
				break;
			}
		}
	}
	if ( ! endPt) {
		Learn2JS.app().outputError(null, "Warning: Never wrote a cell in row "+endRow);
		endPt = [Dice.rnd( Math.floor((size[0]-2)/2) )*2+1, endRow]; // south
	}
	let g = map.gridAt([map.startPt[0], map.startPt[1]-1]);
	g.t = Terrain.forName("stairsUp");
	g = map.gridAt([endPt[0], endPt[1]+1]);
	g.t = Terrain.forName("stairsDown");

	return map;
}

function generateDiag(size) {
	const map = new GridMap(size);
	const terFloor = Terrain.forName("floor");
	const featDiagNE = Terrain.forName("diagNE");
	const featDiagSE = Terrain.forName("diagSE");

	for (let y = 0; y < size[1]; ++y) {
		for (let x = 0; x < size[0]; ++x) {
			const g = map.gridAt([x,y]);
			g.t = terFloor;
			const roll = Dice.rnd(100);
			if (roll <= 50) {
				g.f = featDiagNE;
			} else {
				g.f = featDiagSE;
			}
		}
	}

	return map;
}

function generateForest(size) {
	const map = new GridMap(size);
	const terGrass = Terrain.forName("grass");
	const terHill = Terrain.forName("hill");
	const terMountain = Terrain.forName("mountain");

	const featTree1 = Terrain.forName("tree");
	const featTree2 = Terrain.forName("treeAutumn");
	const featTree3 = Terrain.forName("treeDead");
	const featRuin = Terrain.forName("ruin");
	const featPit = Terrain.forName("pitFound");
	const featRiver = Terrain.forName("river_nesw");
	const featPath = Terrain.forName("path_nesw");

	for (let y = 0; y < size[1]; ++y) {
		for (let x = 0; x < size[0]; ++x) {
			const g = map.gridAt([x,y]);
			let roll = Dice.rnd(100);
			if (roll <= 80) {
				g.t = terGrass;
			} else if (roll <= 92) {
				g.t = terHill;
			} else {
				g.t = terMountain;
				// no feature on mountain
				continue;
			}
			roll = Dice.rnd(100);
			if (roll <= 33) {
				// no feature
			} else if (roll <= 66) {
				g.f = featTree1;
			} else if (roll <= 84) {
				g.f = featTree2;
			} else if (roll <= 96) {
				g.f = featTree3;
			} else if (roll <= 98) {
				g.f = featRuin;
			} else {
				g.f = featPit;
			}
		}
	}

	walkRandom(map, null, featRiver);
	walkRandom(map, null, featPath);

	return map;
}

function walkRandom(map, t, f) {
	const walkStart = Dice.boxSide(MathUtil.rectMake([0, 0], map.size));
	let pt = MathUtil.pointCopy(walkStart);
	let dir = Dir.forValue( Dice.rnd(4) );
	if ( ! map.inBounds(MathUtil.pointAdd(pt, dir.delta)) ) {
		dir = dir.backDir();
	}
	const dirStart = dir;
	for (let i = 0; i < Math.max(map.size[0], map.size[1]) * 32; ++i) {
		const g = map.gridAt(pt);
		if (g && walkPassable(g.t) && walkPassable(g.f)) {
			if (t) {
				g.t = t;
			}
			if (f) {
				g.f = f;
			}
		}
		const turnRoll = Dice.rnd(100);
		if (turnRoll <= 83) {
			// forward
		} else if (turnRoll <= 92) {
			dir = dir.leftDir();
		} else {
			dir = dir.rightDir();
		}
		if (dir === dirStart.backDir()) { // don't go backwards
			dir = dir.backDir();
		}
		const pt2 = MathUtil.pointAdd(pt, dir.delta);
		const g2 = map.gridAt(pt2);
		if ( ! g2 && MathUtil.pointDist(walkStart, pt2) >= Math.max(map.size[0], map.size[1]) ) {
			break;
		}
		if (g2 && walkPassable(g2.t) && walkPassable(g2.f)) {
			pt = pt2;
		}
	}
}

function walkPassable(ter) {
	if ( ! ter) {
		return true; // accepted so no-feature is OK, grid is tested before this.
	}
	switch (ter.name) {
		case "mountain":
		case "ruin":
		case "pit":
		case "pitFound":
		case "river_nesw":
		case "path_nesw":
			return false;
		default:
			return true;
	}
}

function generateIsland(size) {
	const terWater = Terrain.forName("water");
	const map = new GridMap(size);
	map.filter((map, pt, g, h)=>{
		g.t = terWater;
		g.a = 0;
		return true;
	});
	const wd = Math.floor(size[0]/2), hd = Math.floor(size[1]/2);
	for (let i = 0; i < size[0] * size[1] * 16; ++i) {
		const pt = [Dice.d(2, wd)-1, Dice.d(2, hd)-1];
		const g = map.gridAt(pt);
		g.a = (g.a||0)+1;
	}

	map.filter(generateIslandFilterLand);
	map.filter(generateWorldFilterLand);
	map.filter(generateWorldFilterMountain);

	return map;
}

function generateIslandFilterLand(map, pt, g, h) {
	const terWaterDeep = Terrain.forName("waterDeep");
	const terWater = Terrain.forName("water");
	const terGrass = Terrain.forName("grass");
	const terMountain = Terrain.forName("mountain");

	if (g.a <= 16) {
		g.t = terWaterDeep;
	} else if (g.a < 32) {
		g.t = terWater;
	} else if (g.a > 64) {
		g.t = terMountain;
	} else {
		g.t = terGrass;
	}
}

function generateWorld(size) {
	let step = MathUtil.roundPowerOf2(Math.max(size[0], size[1]));
	if (step !== size[0] || step !== size[1]) {
		Learn2JS.app().output("<div>Resizing to "+step+","+step+"</div>\n");
		size = [step, step];
	}
	const map = new GridMap(size);

	const terWater = Terrain.forName("water");
	const terGrass = Terrain.forName("grass");

	map.fillTerrain([0, 0, size[0], size[1]], terWater);
	step >>= 1;
	map.startPt = [step, step];
	map.gridAt(map.startPt).t = terGrass;
	generateWorldFractal(map, step>>1);

	map.filter(generateWorldFilterLand);
	map.filter(generateWorldFilterMountain);

	return map;
}

function generateWorldFractal(map, step) {
	DLOG("genWorldFractal", step);
	const terWater = Terrain.forName("water");
	for (let y = 0; y < map.size[1]; y += step) {
		for (let x = 0; x < map.size[0]; x += step) {
			// add random offsets
			let cx = x, cy = y;
			if (Dice.rnd(2) === 1) {
				cx += step;
			}
			if (Dice.rnd(2) === 1) {
				cy += step;
			}
			// Truncate to nearest multiple of step*2,
			// since step*2 is the previous detail level calculated.
			cx = Math.floor(cx / (step+step)) * (step+step);
			cy = Math.floor(cy / (step+step)) * (step+step);
			// Read from the randomized cell.
			// Assume the world beyond the boundaries is nothing but endless water.
			let t;
			if (map.inBounds([cx,cy])) {
				t = map.gridAt([cx,cy]).t;
			} else {
				t = terWater;
			}
			map.gridAt([x,y]).t = t;
		}
	}
	// Generate finer details until we reach the unit scale.
	if (step > 1) {
		generateWorldFractal(map, step>>1);
	}
}

function generateWorldFilterLand(map, pt, g, h) {
	const terWaterDeep = Terrain.forName("waterDeep");
	const terWater = Terrain.forName("water");
	const terGrass = Terrain.forName("grass");
	const terSand = Terrain.forName("sand");
	const terHill = Terrain.forName("hill");
	const featTree = Terrain.forName("tree");

	if (g.t === terWater || g.t === terWaterDeep) {
		return;
	} else if ((h[terWater]||0)+(h[terWaterDeep]||0) > 0) {
		const roll = Dice.rnd(100);
		if (roll <= 40) {
			g.t = terSand;
		} else if (roll <= 50) {
			g.t = terGrass;
			g.f = featTree;
		} else {
			// pass
		}
	} else {
		const roll = Dice.rnd(100);
		if (roll <= 33) {
			g.t = terGrass;
			g.f = featTree;
		} else if (roll <= 66) {
			g.t = terHill;
		} else {
			// pass
		}
	}
}

function generateWorldFilterMountain(map, pt, g, h) {
	const terHill = Terrain.forName("hill");
	const terMountain = Terrain.forName("mountain");

	if (g.t === terHill && (h[terHill]||0)+(h[terMountain]||0) > 3) {
		g.t = terMountain;
	}
}

function init() {
	DLOG("maze init");
	Terrain.preloadImages();

	Canvas.reset();
	Learn2JS.app().outputClear();
	Learn2JS.app().output("<h2>MAZE</h2>\n");
	//Learn2JS.app().output(kMazeHelp);
	Learn2JS.app().output(kMazeUI);
	Learn2JS.app().inputFocus();
}

function parseSize(widthStr, heightStr) {
	const size = [0, 0];
	size[0] = StringUtil.parseInteger(widthStr, 2, 128);
	size[1] = StringUtil.parseInteger(heightStr, 2, 128);
	return size;
}

function unload() {
	DLOG("maze unload");
	Canvas.reset();
}

function main(line) {
	DLOG("maze main: ", line);
	Learn2JS.app().output("<div class='prompt'>"+StringUtil.htmlify("> "+line)+"</div>\n");
	let map = null;

	const words = StringUtil.splitWords(line);
	try {
		if (words.length === 0) {
			return;
		} else if (words[0] === "help") {
			Learn2JS.app().output(kMazeHelp);
			return;
		} else if (words[0] === "menu") {
			Learn2JS.app().outputClear();
			Learn2JS.app().output(kMazeUI);
			return;
		} else if (words[0] === "text") {
			if (LAST_MAP) {
				Canvas.reset();
				Learn2JS.app().outputClear();
				Learn2JS.app().output("<pre>"+StringUtil.htmlify(LAST_MAP.toString())+"</pre>\n");
				return;
			} else {
				Learn2JS.app().outputError(null, "No previous map!");
				return;
			}
		} else if (words[0] === "redraw") {
			if (LAST_MAP) {
				map = LAST_MAP;
			} else {
				Learn2JS.app().outputError(null, "No previous map!");
				return;
			}
		} else if (words[0] === "maze" && words.length === 3) {
			map = generateMaze( parseSize(words[1], words[2]) );
		} else if (words[0] === "diag" && words.length === 3) {
			map = generateDiag( parseSize(words[1], words[2]) );
		} else if (words[0] === "forest" && words.length === 3) {
			map = generateForest( parseSize(words[1], words[2]) );
		} else if (words[0] === "island" && words.length === 3) {
			map = generateIsland( parseSize(words[1], words[2]) );
		} else if (words[0] === "world" && words.length === 3) {
			map = generateWorld( parseSize(words[1], words[2]) );
		} else {
			Learn2JS.app().outputError(null, kMazeError);
			return;
		}
	} catch (e) {
		Learn2JS.app().outputError(e);
		return;
	}

	if (map) {
		if (words.length === 3) {
			$("mazetype").value = words[0];
			$("mazewidth").value = words[1];
			$("mazeheight").value = words[2];
		}
		LAST_MAP = map;
		const canvas = Canvas.canvas();
		canvas.clear();
		const scale = canvas.canvasSize()[1] / map.size[1];
		map.draw(canvas, [0, 0], scale >= 32 ? 32 : Math.floor(scale));
	}
}

module.exports = { init, main, unload };
