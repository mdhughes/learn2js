/* alien.js
 * Learn2JS
 * Copyright © 2018 Mark Damon Hughes
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following adisclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/* eslint-env node */

"use strict";

const { $, DEBUG, DLOG, Learn2JS, FPSCounter } = require("./Learn2JS.js");
const { ArrayUtil } = require("./ArrayUtil.js");
const { AudioPlayer } = require("./AudioPlayer.js");
const { Canvas, Sprite } = require("./Canvas.js");
const { Dice } = require("./Dice.js");
const { MathUtil } = require("./MathUtil.js");
const { StringUtil } = require("./StringUtil.js");

const kMoveDelay = 10; // ms between move commands
const kFireDelay = 500; // ms between fire commands
const kDamageDelay = 500; // ms between taking damage

const kPlayerSize = 32;
const kAlienSize = 32;
const kMissileSize = 8;

let LAST_MOVE_TIME = 0;
let LAST_FIRE_TIME = 0;
let LAST_DAMAGE_TIME = 0;
let PLAYER = null;
let ALIENS = [];
let PLMISSILES = [];

function doFire() {
	DLOG("fire");
	const pt = MathUtil.pointAdd(PLAYER.origin, [kPlayerSize/2, kPlayerSize/2]);
	const missile = new Sprite("plmsl-"+Date.now(), pt, [kMissileSize, kMissileSize]);
	missile.color = "white";
	missile.speed = 8;
	missile.delta = [Math.sign(PLAYER.delta[0]) * missile.speed, Math.sign(PLAYER.delta[1]) * missile.speed];
	missile.damage = 4;
	PLMISSILES.push(missile);
	// FIXME: fire noise
}

function doMove(delta) {
	DLOG("move", delta[0], delta[1]);
	const newpt = MathUtil.pointAdd(PLAYER.origin, delta);
	const newbounds = MathUtil.rectMake(newpt, PLAYER.size);
	const canvas = Canvas.canvas();
	const canvasSize = canvas.canvasSize();
	if ( ! MathUtil.rectContainsRect(MathUtil.rectMake([0, 0], canvasSize), newbounds)) {
		// FIXME: border noise
		doTakeDamage(1);
		return;
	}

	PLAYER.origin = newpt;
	PLAYER.delta = delta;
}

function doTakeDamage(n) {
	const canvas = Canvas.canvas();
	if (Learn2JS.app().timerTimestamp >= LAST_DAMAGE_TIME + kDamageDelay) {
		LAST_DAMAGE_TIME = Learn2JS.app().timerTimestamp;
		PLAYER.health -= 1;
		if (PLAYER.health <= 0) {
			gameOver();
		}
	}
}

function gameOver() {
	Learn2JS.app().timerStop();
	redraw();
	Learn2JS.app().outputClear();
	Learn2JS.app().output("<h2>ALIEN</h2> <h3>GAME OVER</h3> <div>Type 'alien' to restart.</div>\n");
	Learn2JS.app().inputFocus();
}

function update() {
	updatePlayer();
	updateMissiles();
	updateAliens();
	redraw();
}

function updateAliens() {
	const canvasSize = Canvas.canvas().canvasSize();

	// create new aliens along the edge
	if (ALIENS.length === 0 || Dice.rnd(3600) < 10) {
		const side = Dice.rnd(4);
		let pt;
		switch (side) {
			case 0:
				pt =[ Dice.rnd(canvasSize[0] / kAlienSize) * kAlienSize, 0 ];
				break;
			case 1:
				pt = [ canvasSize[0]-kAlienSize, Dice.rnd(canvasSize[1]/kAlienSize)*kAlienSize ];
				break;
			case 2:
				pt = [ Dice.rnd(canvasSize[0]/kAlienSize)*kAlienSize, canvasSize[1]-kAlienSize ];
				break;
			case 3:
				pt = [ 0, Dice.rnd(canvasSize[1]/kAlienSize)*kAlienSize ];
				break;
		}
		const alien = new Sprite("alien-"+Date.now(), pt, [kAlienSize, kAlienSize]);
		const speciesRoll = ALIENS.length < 4 ? Dice.rnd(60) : Dice.rnd(100);
		if (speciesRoll <= 50) {
			// normal
			alien.color = "magenta";
			alien.speed = 3;
			alien.healthMax = 5;
			alien.health = 5;
			alien.evasion = 20;
			alien.score = 10;
		} else if (speciesRoll <= 75) {
			// scout
			alien.color = "orange";
			alien.speed = 4;
			alien.healthMax = 3;
			alien.health = 3;
			alien.evasion = 30;
			alien.score = 5;
		} else {
			// heavy
			alien.color = "red";
			alien.speed = 2;
			alien.healthMax = 10;
			alien.health = 10;
			alien.evasion = 10;
			alien.score = 15;
		}
		alien.target = null;
		ALIENS.push(alien);
	}

	// move aliens
	for (let i = ALIENS.length-1; i >= 0; --i) { // count backwards so we can remove elements as we go
		const alien = ALIENS[i];
		if (alien.health <= 0) {
			PLAYER.score += alien.score;
			// FIXME: explosion sound & sprite
			ALIENS.splice(i, 1);
			continue;
		}
		const roll = alien.target ? Dice.rnd(3600) : Dice.rnd(60);
		if (roll < 10) {
			const evasionRoll = Dice.rnd(100);
			if (evasionRoll <= alien.evasion) {
				// evade to random screen loc
				alien.target = [ Dice.rnd(canvasSize[0] / kAlienSize) * kAlienSize, Dice.rnd(canvasSize[1]/kAlienSize)*kAlienSize ];
			} else {
				// chase player
				alien.target = MathUtil.pointCopy(PLAYER.origin);
			}
		}

		if (alien.target) {
			const delta = [0, 0];
			if (alien.origin[0] < alien.target[0]) {
				delta[0] += alien.speed;
			} else if (alien.origin[0] > alien.target[0]) {
				delta[0] -= alien.speed;
			}
			if (alien.origin[1] < alien.target[1]) {
				delta[1] += alien.speed;
			} else if (alien.origin[1] > alien.target[1]) {
				delta[1] -= alien.speed;
			}

			// stop on collision with other aliens
			let newpt = MathUtil.pointAdd(alien.origin, delta);
			for (const a2 of ALIENS) {
				if (alien !== a2 && MathUtil.rectIntersectsRect(MathUtil.rectMake(newpt, alien.size), a2.bounds() )) {
					newpt = alien.origin;
					alien.target = null;
					break;
				}
			}
			alien.origin = newpt;

			// stop when we get there, restart next tick
			if (alien.target && MathUtil.pointDist(alien.origin, alien.target) <= kAlienSize) {
				alien.target = null;
			}
		}

	}
}

function updateMissiles() {
	const canvas = Canvas.canvas();
	const canvasBounds = MathUtil.rectMake([0, 0], canvas.canvasSize());
	for (let i = PLMISSILES.length-1; i >= 0; --i) { // count backwards so we can remove elements as we go
		const msl = PLMISSILES[i];
		msl.origin = MathUtil.pointAdd(msl.origin, msl.delta);
		for (const alien of ALIENS) {
			if ( ! alien.destroyed && MathUtil.rectIntersectsRect(alien.bounds(), msl.bounds() )) {
				alien.health -= msl.damage;
				msl.destroyed = true;
				break;
			}
		}
		if ( ! MathUtil.rectContainsRect(canvasBounds, msl.bounds() )) {
			msl.destroyed = true;
		}
		if (msl.destroyed) {
			PLMISSILES.splice(i, 1);
		}
	}
}

function updatePlayer() {
	const app = Learn2JS.app(), canvas = Canvas.canvas();
	const delta = [0, 0];
	if (app.keyState.up || app.keyState.arrowup || app.keyState.w) {
		delta[1] -= PLAYER.speed;
	}
	if (app.keyState.down || app.keyState.arrowdown || app.keyState.s) {
		delta[1] += PLAYER.speed;
	}
	if (app.keyState.left || app.keyState.arrowleft || app.keyState.a) {
		delta[0] -= PLAYER.speed;
	}
	if (app.keyState.right || app.keyState.arrowright || app.keyState.d) {
		delta[0] += PLAYER.speed;
	}
	if ( (delta[0] !== 0 || delta[1] !== 0) && app.timerTimestamp >= LAST_MOVE_TIME + kMoveDelay) {
		LAST_MOVE_TIME = app.timerTimestamp;
		doMove(delta);
	}
	if (app.keyState.shift) {
		if (app.timerTimestamp >= LAST_FIRE_TIME + kFireDelay) {
			LAST_FIRE_TIME = app.timerTimestamp;
			doFire();
		}
	}

	const plbounds = PLAYER.bounds();
	for (const alien of ALIENS) {
		if (MathUtil.rectIntersectsRect(plbounds, alien.bounds() )) {
			// FIXME: alien contact noise
			doTakeDamage(1);
		}
	}
}

function redraw() {
	const canvas = Canvas.canvas();
	const canvasSize = canvas.canvasSize();
	canvas.clear();
	if (PLAYER.health > 0) {
		// draw background
	} else {
		canvas.setColor("#330000");
		canvas.fillRect(MathUtil.rectMake([0, 0], canvasSize));
	}

	for (const alien of ALIENS) {
		alien.draw();
	}
	for (const msl of PLMISSILES) {
		msl.draw();
	}
	PLAYER.draw();

	const lifeColor = Sprite.colorForHealthFraction(PLAYER.health / PLAYER.healthMax);
	canvas.drawTextFont("❤️ "+PLAYER.health+" / "+PLAYER.healthMax, "bold 16pt Fira Mono", lifeColor, [4, 4], "left", "top");
	canvas.drawTextFont(StringUtil.padLeft(PLAYER.score, 4, "0")+" 🏆", "bold 16pt Fira Mono", lifeColor, [canvasSize[0]-4, 4], "right", "top");
	canvas.drawTextMono("FPS: "+Learn2JS.app().fpsCounter.fps.toFixed(3), 10, "white", [canvasSize[0]-4, canvasSize[1]-4], "right", "bottom");
}

function init() {
	DLOG("alien init");
	const canvas = Canvas.canvas();
	const canvasSize = canvas.canvasSize();
	Learn2JS.app().timerStop();

	Learn2JS.app().outputClear();
	Learn2JS.app().output("<h2>ALIEN</h2> <div>Move: Arrows <b>↑ ↓ ← →</b> or <b>W A S D</b>\n<br>Fire: <b>Shift</b></div>\n");

	PLAYER = new Sprite("player", [Math.floor(canvasSize[0] / 2), Math.floor(canvasSize[1] / 2)], [kPlayerSize, kPlayerSize]);
	PLAYER.color = "yellow";
	PLAYER.speed = 4;
	PLAYER.healthMax = 10;
	PLAYER.health = 10;
	PLAYER.delta = [1, 0];
	PLAYER.score = 0;

	ALIENS = [];
	PLMISSILES = [];

	Learn2JS.app().timerStart(update);
	canvas.element.focus();
}

function unload() {
	DLOG("alien unload");
	Learn2JS.app().timerStop();
	Canvas.reset();
}

function main(line) {
	DLOG("alien main", line);
	if (line.toLowerCase() === "alien") {
		init();
	}
}

module.exports = { init, main, unload };
