#!/bin/zsh

git log >CHANGELOG

cd app
jsStrictCheck.zsh
if [[ $? != 0 ]]; then
	echo "Terminating"
	exit 1
fi
cd ..

npm run build && npm run dist-mac
