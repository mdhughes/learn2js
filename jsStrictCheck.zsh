#!/bin/zsh
err=0
for f in **/*.js; do
	grep "\/\* eslint-env" "$f" >/dev/null
	if [[ $? != 0 ]]; then
		echo "$f: Missing /* eslint-env node */"
		err=1
	fi
	grep "use strict" "$f" >/dev/null
	if [[ $? != 0 ]]; then
		echo "$f: Missing \"use strict\";"
		err=1
	fi
done
exit $err
